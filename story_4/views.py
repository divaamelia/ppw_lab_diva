from django.shortcuts import render

# Create your views here.
def home(request):
    return render(request, 'home.html')

def skills(request):
    return render(request, 'skills.html')

def education(request):
    return render(request, 'education.html')

def experience(request):
    return render(request, 'experience.html')

def project(request):
    return render(request, 'project.html')

def contact(request):
    return render(request, 'contact.html')