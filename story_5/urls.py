from django.urls import path

from . import views

urlpatterns = [
    path('', views.schedule, name='schedule'),
    path('create/', views.schedule_create, name='schedule_create'),
    path('clear/<int:id>/', views.schedule_clear, name='schedule_clear'),
]
