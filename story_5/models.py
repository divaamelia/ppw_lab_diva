from django.db import models
from datetime import datetime, date

class Schedule(models.Model):
    name = models.CharField(max_length = 20)
    date = models.DateField(default = datetime.now)
    time = models.TimeField()
    activity = models.CharField(max_length=20)
    place = models.CharField(max_length = 20)
    category = models.CharField (max_length = 20)

